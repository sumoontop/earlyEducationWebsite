# earlyeducationwebsite

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### docker镜像
```
更新时间：2021/11/23 16:13:04 GMT+08:00
大小：14.8MB
下载指令：docker pull swr.cn-north-4.myhuaweicloud.com/sumoon/earlyedu_v1.0.101:latest
```

