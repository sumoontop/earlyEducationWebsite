::停止镜像
docker stop swr.cn-north-4.myhuaweicloud.com/sumoon/earlyedu_v1.0.100

::打包镜像
docker build -t swr.cn-north-4.myhuaweicloud.com/sumoon/earlyedu_v1.0.101 .

::tag镜像
::docker tag earlyedu_v1.0.101 swr.cn-north-4.myhuaweicloud.com/sumoon/earlyedu_v1.0.101:latest
::删除镜像
::docker rm -f earlyedu_v1.0.101
 
::推镜像
docker push swr.cn-north-4.myhuaweicloud.com/sumoon/earlyedu_v1.0.101:latest
 
::展示镜像
docker images

::部署镜像
docker run -d -p 80:80 --name earlyeducation earlyedu_v1.0.101

::查看容器
docker ps
 
pause