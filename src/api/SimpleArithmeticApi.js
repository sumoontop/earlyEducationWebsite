import axios from 'axios';

export function getSimpleArithmeticProblems(type, maxNumber) {
  return axios.get('http://192.168.31.136:8081/simpleArithmetic/problems', {params: { type, maxNumber }});
}
