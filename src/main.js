import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 统一导入el-icon图标
import * as ElIconModules from '@element-plus/icons'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

import './assets/css/common.css'
import './assets/css/index.css'
import './assets/css/app.css'
import './assets/css/math.css'

const app = createApp(App);

axios.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  res.header('X-Powered-By', '3.2.1')
  res.header('Content-Type', 'application/json;charset=utf-8');
  next();
});

// 统一注册el-icon图标
for(let iconName in ElIconModules) {
  app.component(transElIconName(iconName), ElIconModules[iconName])
}

app.use(axios)
  .use(ElementPlus, {
    locale: zhCn
  })
  .use(router)
  .use(store)
  .use(router)
  .mount('#app')

function transElIconName(iconName) {
  return 'i' + iconName.replace(/[A-Z]/g, (match)=>'-' + match.toLowerCase())
}
