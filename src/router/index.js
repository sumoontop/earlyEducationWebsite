import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Number from '../views/Number.vue'
import Math from '../views/Math.vue'
import AbcLetter from '../views/AbcLetter.vue'
import About from '../views/About.vue'

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: { title: '首页' }
  },
  {
    path: '/number',
    name: 'Number',
    component: Number,
    meta: { title: '数字' }
  },
  {
    path: '/math',
    name: 'Math',
    component: Math,
    meta: { title: '四则运算' }
  },
  {
    path: '/letter',
    name: 'AbcLetter',
    component: AbcLetter,
    meta: { title: '拼音字母' }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: { title: '关于' }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
