# 常用docker操作
## 登录仓库
```	
docker login docker-registry.xxxx (用户名不带mail)
```
## 退出仓库	
```
docker logout
```
## 拉取镜像	
```
docker pull docker-registry.xxx/docker/nginx:1.13.6
```
## 推送镜像	
```
docker push
```
## 删除镜像	
```
docker rmi IMAGE ID
```
## 查看正在运行的容器	
```
docker ps
```
## 停止一个容器	
```
docker stop CONTAINER ID
```
## 查看所有容器	
```
docker ps -a
```
## 删除一个容器	
```
docker rm CONTAINER ID
```
## 删除所有容器	
```
docker rm $(docker ps -a -q)
```
## 容器挂载	
```
docker -v /opt/www/html:/var/www/html 宿主机绝对路径(一定要是绝对路径，不然无法挂载):docker容器
```
## build容器	
```
docker build -t fgf/nginx:beta_v1 . (一定要有.，并且在Dockfile所在文件夹操作，也可以做其他设置，简单点就行)
```
## run	
```
docker run -d -p 80:80 IMAGE ID(-d 后台运行 -p 宿主机端口:docker端口)
```
## 进入一个正在运行的docker	
```
docker exec -it CONTAINER ID /bin/bash
```
## 进入一个镜像	
```
docker run -it IMAGE ID /bin/bash
```
## 从容器中退出	
```
Ctrl + d 或者exit回车
```
## 镜像重命名	
```
docker tag IMAGE ID 新名字
```