# 设置基础镜像 
FROM nginx:alpine
# 定义作者
MAINTAINER wuyang
# 将dist文件中的内容复制到 /etc/nginx/html/ 这个目录下面
COPY dist/  /usr/share/nginx/html
# 将配置文件中的内容复制到 /etc/nginx 这个目录下面(增加自己的代理及一些配置)
RUN rm -rf /usr/share/nginx/nginx.conf 
COPY nginx.conf /usr/share/nginx/nginx.conf