#停止镜像
docker stop swr.cn-east-2.myhuaweicloud.com/sumoon/earlyedu_v1.0.101
#tag镜像
#docker tag earlyedu_v1.0.101 swr.cn-east-2.myhuaweicloud.com/sumoon/earlyedu_v1.0.101:latest
#删除镜像
#docker rm -f earlyedu_v1.0.101
#拉取代码
git pull https://gitee.com/sumoontop/earlyEducationWebsite.git
#npm构建
npm run build
#打包镜像
docker build -t swr.cn-east-2.myhuaweicloud.com/sumoon/earlyedu_v1.0.101 .
#推镜像
docker push swr.cn-east-2.myhuaweicloud.com/sumoon/earlyedu_v1.0.101:latest
#展示镜像
docker images
#部署镜像
docker run -p 80:80 -d --name earlyedu_v1.0.101 swr.cn-east-2.myhuaweicloud.com/sumoon/earlyedu_v1.0.101
#查看容器
docker ps
pause