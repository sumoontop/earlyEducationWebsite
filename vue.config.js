module.exports = {
  assetsDir: 'public',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = '早教平台'
        return args
      })
  },
  devServer: {
    proxy: {
      '/': {
        target: 'http://192.168.31.136:8081/', // 你接口的域名 
        //secure: false,      // 如果是https接口，需要配置这个参数
        ws: true,
        changeOrigin: true,     // 如果接口跨域，需要进行这个参数配置
        pathRewrite: {
          '^/': ''
        }
      }
    }
  }
}